﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using testProject.Models;
//including this for the extra Project definition
using testProject.Models.ViewModels;
using System.Diagnostics;
//Reference: As per Christine's Code
namespace testProject.Controllers
{
    public class CategoryController : Controller
    {
        // variables which holds our database
        private ProductCMSContext db = new ProductCMSContext();

        // GET: Category
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

       
        // GET: Category New
        public ActionResult New()
        {
            CategoryEdit categoryeditview = new CategoryEdit();
            return View(categoryeditview);
        }

        // POST: Category New

        [HttpPost]
        public ActionResult New(string CategoryName_New, string CategoryDescription_New)
        {
            // Standard query representation for Categories to Add a new Category 
            string query = "insert into categories (CategoryName, CategoryDescription) values (@name, @description)";

            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@name", CategoryName_New);
            myparams[1] = new SqlParameter("@description", CategoryDescription_New);
          
           
            // By Using, This Command puts that Informtion into Database
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }


        // GET: Category Edit
        public ActionResult Edit(int id)
        {
           

            CategoryEdit categoryeditview = new CategoryEdit();

            // Line equivalent to "Select * from Categories where categoryid = .."
            // It will find one particular CategoryID form Database 
            categoryeditview.category = db.Categories.Find(id);

            //and it returns a view of Edit Category
            return View(categoryeditview);
        }


        // POST: Category Edit
        [HttpPost]
        public ActionResult Edit(int id, string CategoryName, string CategoryDescription)
        {
            //If the ID doesn't exist or the Category doesn't exist
            if ((id == null) || (db.Categories.Find(id) == null))
            {
                return HttpNotFound();

            }

            // update query for List of Categories
            string query = "update categories set CategoryName=@name, CategoryDescription=@description where categoryid=@id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name", CategoryName);
            myparams[1] = new SqlParameter("@description", CategoryDescription);
            myparams[2] = new SqlParameter("@id", id);
           
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }


        // Show Category
        public ActionResult Show(int? id)
        {
            //If the ID doesn't exist or the Category doesn't exist
            if ((id == null) || (db.Categories.Find(id) == null))
            {
                return HttpNotFound();

            }

            // query to show the List of all categories 
            string query = "select * from categories where categoryid=@id";
            
            //Could also just pass the sqlparameter by itself instead of making an array of 1 item
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            //Hey view.. here's a list of all the categories in my db
            Category mycategory = db.Categories.SqlQuery(query, myparams).FirstOrDefault();

            return View(mycategory);
        }


        // Delete a Category
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Categories.Find(id) == null))
            {
                return HttpNotFound();

            }
            

            // to delete associated Products
            string query = "delete from Products where category_CategoryID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            // to delete Category
            query = "delete from Categories where categoryid=@id";
            param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            return RedirectToAction("List");
        }


        // List of Categories
        public ActionResult List()
        {
            // This function needs to print out a list of the categories

            IEnumerable<Category> categories = db.Categories.ToList();
            return View(categories);
        }
    }
}