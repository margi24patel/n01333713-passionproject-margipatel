﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using testProject.Models;
//including this for the extra Project definition
using testProject.Models.ViewModels;
using System.Diagnostics;

//Reference: As per Christine's Code
namespace testProject.Controllers
{
    public class ProductController : Controller
    {
        //variable which holds our Database

        private ProductCMSContext db = new ProductCMSContext();
        
        
        // GET: Product
        public ActionResult Index()
        {
            // It redirect to the list view method for the Product
            return RedirectToAction("List");
        }
        
        // GET: New Product
        public ActionResult New()
        {
            return View(db.Categories.ToList());
        }

        // POST: New Product
        [HttpPost]
        public ActionResult New(string ProductName_New, string ProductDescription_New, float ProductPrice_New, int? ProductCategory_New) // Created a New variables for fields
        {
            // query to Add new Product with using CategoryID to select a Category for that particular Product
            string query = "insert into Products (ProductName, ProductDescription, ProductPrice, category_CategoryID) " +
                "values (@name,@description,@price,@cid)";

            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@name", ProductName_New);
            myparams[1] = new SqlParameter("@description", ProductDescription_New);
            myparams[2] = new SqlParameter("@price", ProductPrice_New);
            myparams[3] = new SqlParameter("@cid", ProductCategory_New);

            db.Database.ExecuteSqlCommand(query, myparams);
            
            return RedirectToAction("List");
        }

       
        // GET: Edit Product
        public ActionResult Edit(int? id)
        {
           
            ProductEdit productedit = new ProductEdit();
            productedit.product = db.Products.Find(id);
            productedit.categories = db.Categories.ToList();
            
            if (productedit.product != null) return View(productedit);
            else return HttpNotFound();
        }


        // POST: Edit Product
        [HttpPost]
        public ActionResult Edit(string ProductName, string ProductDescription, float? ProductPrice, int? ProductCategory, int? id)
        {
            //to find error 
            //Debug.WriteLine("");
            if ((id == null) || (db.Products.Find(id) == null))
            {
                return HttpNotFound();

            }
            // query to update a particular Product
            string query = "update products set productname=@name, " +
                "productdescription=@description, " +
                "productprice=@price, " +
                "category_CategoryID=@cid where productid = @id";

            SqlParameter[] myparams = new SqlParameter[5];

            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@name";
            myparams[0].Value = ProductName;

            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@description";
            myparams[1].Value = ProductDescription;

            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@price";
            myparams[2].Value = ProductPrice;

            myparams[3] = new SqlParameter();
            myparams[3].ParameterName = "@cid";
            myparams[3].Value = ProductCategory;

            myparams[4] = new SqlParameter();
            myparams[4].ParameterName = "@id";
            myparams[4].Value = id;
            db.Database.ExecuteSqlCommand(query, myparams);
           

            return RedirectToAction("Show/" + id);
        }

        // Show Product
        public ActionResult Show(int id)
        {
            string query = "select * from products where productid =@id";

            //should return type of "Product"

            // debug used for to check error
            Debug.WriteLine(query);
            return View(db.Products.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }


        //Delete a Product
        public ActionResult Delete(int id)
        {
           
            // query to delete a particular Product 
            string query = "delete from Products where productid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));


            return RedirectToAction("List");
        }


        //List of Product
        public ActionResult List()
        {

            //it returns view for List of Products
            return View(db.Products.ToList());

        }
    }
}