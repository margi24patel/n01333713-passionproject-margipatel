﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace testProject.Models
{
    public class Category
    {
        //One category to many Products

         //For Category ID
        [Key,ScaffoldColumn(false)]
        public int CategoryID { get; set; }


        //labelled for CategoryName
        [Required, StringLength(255), Display(Name ="Name")]
        public string CategoryName { get; set; }


        // labelled For Category Description
        [StringLength(int.MaxValue), Display(Name ="Description")]
        public string CategoryDescription { get; set; }

        //for one Category have many Products
        public virtual ICollection<Product> Products { get; set; }

       
    }
} 