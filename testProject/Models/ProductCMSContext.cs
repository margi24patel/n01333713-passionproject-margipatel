﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace testProject.Models
{
    //Subclass of DbContext
    public class ProductCMSContext : DbContext
    {
        
            public ProductCMSContext()
            {

            }

            public DbSet<Product> Products { get; set; }
            public DbSet<Category> Categories { get; set;}
    }
}