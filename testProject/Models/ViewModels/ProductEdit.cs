﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testProject.Models.ViewModels
{
    public class ProductEdit
    {

        //Empty constructor
        public ProductEdit()
        {

        }

        //Raw page information (in Models/Page.cs)
        public virtual Product product { get; set; }

        //need information about the different categories this page COULD be
        //assigned to
        public IEnumerable<Category> categories { get; set; }
    }
}