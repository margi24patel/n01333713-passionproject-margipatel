﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace testProject.Models
{
    public class Product
    {
        //one Category has many Products

            //For Product ID
        [Key, ScaffoldColumn(false)] //means use for id bcoz we do not change ID It is like KEY.
        public int ProductID { get; set; }

        //labelled for Product Name
        [Required, StringLength(255), Display(Name = "Name")]
        public string ProductName { get; set; }

        //labelled for Product Description
        [Required, StringLength(int.MaxValue), Display(Name = "Description")]
        public string ProductDescription { get; set; }

        //labelled for Product Price   Reference: https://stackoverflow.com/questions/29975128/asp-net-mvc-data-annotation-for-currency-format
        [Required, DataType(DataType.Currency), Display(Name = "Price")]
        public float ProductPrice { get; set; }

        //for one category has many Products
        public virtual Category category { get; set; }
    }
}